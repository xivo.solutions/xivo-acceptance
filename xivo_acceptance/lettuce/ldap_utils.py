# -*- coding: utf-8 -*-

# Copyright (C) 2013-2016 Avencall
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

import base64
import hashlib

import ldap.modlist
from aloe import world


def sanitize_string(text):
    if isinstance(text, str):
        text = text.encode('utf-8')
    return text


def escape_ldap_string(text):
    return text.replace('\\', '\\\\').replace(' ', '').lower()


def encode_entry(entry):
    return dict((key, sanitize_string(value)) for key, value in entry.items())


def add_or_replace_ldap_entry(entry):
    entry = encode_entry(entry)
    ldap_server = connect_to_server(world.config['ldap']['uri'])

    common_name = entry['cn']
    dn = _get_entry_id(common_name)

    if _ldap_has_entry(ldap_server, common_name):
        delete_entry(ldap_server, common_name)
    add_entry(ldap_server, dn, entry)

    ldap_server.unbind_s()


def generate_ldap_password(password):
    hasher = hashlib.sha1()
    hasher.update(password)
    payload = base64.b64encode(hasher.digest())
    return "{SHA}%s" % payload


def connect_to_server(uri):
    ldap_server = ldap.initialize(uri)
    ldap_server.simple_bind(world.config['ldap']['username'], world.config['ldap']['password'])
    return ldap_server


def _ldap_has_entry(ldap_server, common_name):
    cn = escape_ldap_string(common_name)
    ldap_results = ldap_server.search_s(world.config['ldap']['base_dn'], ldap.SCOPE_SUBTREE, '(cn=%s)' % cn)
    if ldap_results:
        return True
    else:
        return False


def delete_entry(ldap_server, common_name):
    entry_id = _get_entry_id(common_name)
    ldap_server.delete_s(entry_id)


def _get_entry_id(common_name):
    cn = escape_ldap_string(common_name)
    return 'cn=%s,%s' % (cn, world.config['ldap']['base_dn'])


def add_entry(ldap_server, dn, entry):
    entry_encoded = ldap.modlist.addModlist(entry)
    ldap_server.add_s(dn, entry_encoded)
