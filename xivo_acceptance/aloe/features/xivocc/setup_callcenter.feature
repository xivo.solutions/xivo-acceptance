Feature: Setup callcenter

    Scenario: Status since indicator changes when an agent logs in
        Given there are users with infos:
         | firstname | lastname | number | agent_number | context | cti_profile | cti_login | cti_passwd | protocol |
         | acd25     |          | 2571   | 2571         | default | Agent       | acd25     | 0000       | sip      |
         | Super     | 01       |        |              | default | Supervisor  | super01   | 0000       | sip      |
        Given there are queues with infos:
         | name       | display name | number | context | agents_number |
         | enterprise | Enterprise   | 3110   | default | 2571          |
