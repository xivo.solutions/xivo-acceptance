# -*- coding: utf-8 -*-

# Copyright (C) 2013-2016 Avencall
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>


from aloe import world
from xivo_ws.objects.entity import Entity

from xivo_acceptance.lettuce import postgres


def add_entity(name, display_name):
    entity = get_entity_with_name(name)
    if not entity:
        entity = Entity(name=name, display_name=display_name)
        world.ws.entities.add(entity)


def get_entity_with_name(name):
    query = 'SELECT * FROM "entity" WHERE name = \'%s\';' % name
    result = postgres.exec_sql_request(query).fetchone()
    if result:
        return result
    return None


def default_entity_id():
    default_entity_name = world.config['default_entity']
    default_entity = get_entity_with_name(default_entity_name)
    if not default_entity:
        raise Exception('Invalid default entity {}'.format(default_entity_name))
    return default_entity.id
