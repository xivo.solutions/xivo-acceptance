XiVO Acceptance
===============

XiVO Acceptance is a testing framework for running automated tests on a XiVO server. These tests are used for fixing
regressions and testing features before releasing a new version of XiVO.


Requirements
============

We recommend running tests on a dedicated debian machine. Run the following commands to install requirements:

    apt-get install libsasl2-dev xvfb xserver-xephyr linphone-nogtk python-dev postgresql-server-dev-all libldap2-dev lsof virtualenv

Asterisk channel driver
-----------------------

Tests require [chan-test](https://gitlab.com/xivo.solutions/chan-test) installed on tested machine.


Linphone
--------

For Linphone to work, you must do:

    adduser jenkins audio  #Jenkins is the user running the tests

XiVO Client
-----------

Tests require a local copy of the [XiVO Client](https://gitlab.com/xivo.solutions/xivo-client-qt)
on the test machine with FUNCTESTS enabled. Here is a quick example on how to install and compile the client:

    git clone git@gitlab.com:xivo.solutions/xivo-client-qt.git
    cd xivo-client-qt
    qmake
    make FUNCTESTS=yes

Firefox, Selenium and Geckodriver compatibility
-----------------------------------------------

For compatibility with Firefox 55.0.3 tests require selenium 3.5.0 (from pip) and geckodriver 0.20.1

To install geckodriver download and unpack it from https://github.com/mozilla/geckodriver/releases and create symlink:

    ln -s /opt/geckodriver-0.20.1 /usr/local/bin/geckodriver

For compatibility with Selenium 2.53 tests require Firefox 46 to be installed. It is available
from https://ftp.mozilla.org/pub/firefox/releases/46.0.1/. If you don't have Firefox installed, just set symbolic link
to folder where you unpacked the downloaded file:

    sudo ln -s /opt/firefox-46/firefox /usr/bin/firefox

In Firefox, open ```about:config```, search for _update_ and set ```app.update.auto``` and ```app.update.enabled``` to
false.

Source code and LDAP
--------------------

Prepare a machine to run Docker containers with LDAP server. Other installed containers may cause conflicts and may need
to be stopped or removed. On the machine run the ```runLdap.sh``` script
(located in xivo-acceptance directory) to install and start the server.

    git clone git@gitlab.com:xivo.solutions/xivo-acceptance.git
    cd xivo-acceptance
    chmod +x runLdap.sh stopLdap.sh ldapSearch.sh
    ./runLdap.sh

If LDAP server works correctly, this will find it's users:

    ldapsearch -x -h localhost -b dc=xivo,dc=org -D "cn=admin,dc=xivo,dc=org" -w secret

Python requirements
-------------------

These steps will create a special folder ```/env``` in the working directory. Python packages will be installed there to
ensure that they won't collide with packages installed for other Python programs. To be run from xivo-acceptance
directory:

    virtualenv env
    source env/bin/activate
    pip install -r requirements.txt

If needed, Virtualenv can be deactivated by command ```deactivate``` and reactivated by ```source env/bin/activate```.


Configuration
=============

Create a yaml local configuration file in ```~/.xivo-acceptance/default``` and redefine only options that need to be
changed. Default options can be found in
```xivo_acceptance/config.py```. Usually, you will only need to change the IP addresses and subnets. For example:

    ;IP address of the XIVO server
    xivo_host: 192.168.0.10

    ;we need to allow access from the test machine to the server.
    ;add a subnet that includes the test machine's IP address
    prerequisites:
        subnets: 
			- 10.0.0.0/8
			- 192.168.0.0/24

    ;LDAP server 
    ldap:
        - host: 127.0.0.1
        - uri: ldap://127.0.0.1:389/

Setup tests and tested machine
==============================

Continue only if all the requirements are installed and configuration file modified. If tests will be run on virtual
machine, **it is highly recommended to make snapshot right after installation _before_ configuring Wizard.**
The Wizard will be configured automatically by _pre_daily_ tests. Replace paths in these commands and **run them in this
order**:

    source env/bin/activate  #if command line doesn't start with: (env), run from xivo-acceptance directory
    python setup.py install  #setting PYTHONPATH should not be needed after this step
    PYTHONPATH=path/to/xivo_acceptance XC_PATH=/path/to/xivo-client-qt/bin lettuce data/features/pre_daily
    PYTHONPATH=path/to/xivo_acceptance ./bin/xivo-acceptance -v -p

Setup tested XiVO for casperjs tests
====================================

    PYTHONPATH=path/to/xivo_acceptance XC_PATH=/path/to/xivo-client-qt/bin lettuce data/features/xivocc

Running tests
=============

    source env/bin/activate  #if command line doesn't start with: (env), run from xivo-acceptance directory

Tests can be found in the ```features``` directory. You can run all tests:

    PYTHONPATH=path/to/xivo_acceptance XC_PATH=/path/to/xivo-client-qt/bin lettuce data/features/daily

Or only a single test file:

    PYTHONPATH=path/to/xivo_acceptance XC_PATH=/path/to/xivo-client-qt/bin lettuce data/features/daily/call_center/queue.feature

Or only a single scenario:

    PYTHONPATH=path/to/xivo_acceptance XC_PATH=/path/to/xivo-client-qt/bin lettuce data/features/daily/call_center/queue.feature -s 6